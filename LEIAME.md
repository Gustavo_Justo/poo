# README #

### What is this repository for? ###

Aplicação CRUD de estoque de produtos de informática baseado nok local onde estagiei. O programas faz as funções 
básicas de inclusão, alteração, exclusão e consulta de dados.


### How do I get set up? ###

Para rodá-lo devidamente, é necessário uma conexão de banco de dados sql para salvar e pesquisar os dados, além de um
conector sql para java jdbc. Vou disponibilizar o código da tabela abaixo e o conector JDBC na aba de downloads.
Para setar o conector JDBC, basta adicionar o .jar nas suas bibliotecas do eclipse/netbens, através do menu "build patch" e
"add external .jar". Para facilitar, e só clonar o projeto diretamente na sua IDE, para você não precisar copiar e colar classe
por classe.

Para a conexão com o BD, foi usado o login "root" e senha "1234". É só alterar para os dados do seu banco local

CREATE TABLE `produtos` (
  `id_Produtos` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(30) NOT NULL,
  `marca` varchar (30) NOT NULL,
  `quantidade` int(11) NOT NULL,
  PRIMARY KEY (`id_Produtos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
