package view;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import jdbc.ConnectionFactory;

/**
 * @author Justo
 */
public class ConsultaView extends javax.swing.JDialog {

    /**
     * form ConsultaView
     */
    public ConsultaView(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        labelHeader = new java.awt.Label();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        LabelProduto = new javax.swing.JLabel();
        LabelQtd = new javax.swing.JLabel();
        TextFieldProduto = new javax.swing.JTextField();
        TextFieldQtd = new javax.swing.JTextField();
        labelHeader1 = new java.awt.Label();
        labelHeader2 = new java.awt.Label();
        labelHeader3 = new java.awt.Label();
        jScrollPane1 = new javax.swing.JScrollPane();
        TableCon = new javax.swing.JTable();
        LabelCon = new javax.swing.JLabel();
        TextFieldCon = new javax.swing.JTextField();
        ButtonCon = new javax.swing.JButton();
        ButtonVoltar = new javax.swing.JButton();
        ButtonExcluir = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        jDialog1.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        labelHeader.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        labelHeader.setName("header"); // NOI18N
        labelHeader.setText("Sistema Gerenciador de Estoque");

        jButton4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton4.setText("Voltar");
        jButton4.setActionCommand("jButton3");
        jButton4.setPreferredSize(new java.awt.Dimension(42, 120));

        jButton5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton5.setText("Limpar");
        jButton5.setActionCommand("jButton4");
        jButton5.setPreferredSize(new java.awt.Dimension(42, 120));

        jButton6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton6.setText("Salvar");
        jButton6.setActionCommand("jButton5");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        LabelProduto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LabelProduto.setText("Produto:");

        LabelQtd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LabelQtd.setText("Quantidade:");

        labelHeader1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        labelHeader1.setName("header"); // NOI18N
        labelHeader1.setText("Sistema Gerenciador de Estoque");

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(91, 91, 91)
                        .addComponent(labelHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 20, Short.MAX_VALUE))
                    .addGroup(jDialog1Layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(LabelQtd, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                            .addComponent(LabelProduto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jDialog1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)
                                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(TextFieldProduto)
                            .addComponent(TextFieldQtd))))
                .addGap(69, 69, 69))
            .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jDialog1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(labelHeader1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelProduto)
                    .addComponent(TextFieldProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelQtd)
                    .addComponent(TextFieldQtd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
            .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jDialog1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(labelHeader1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        labelHeader2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        labelHeader2.setName("header"); // NOI18N
        labelHeader2.setText("Sistema Gerenciador de Estoque");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        labelHeader3.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        labelHeader3.setName("header"); // NOI18N
        labelHeader3.setText("Sistema Gerenciador de Estoque");

        TableCon.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "C�digo", "Produto", "Marca", "Quantidade"
            }
        ));
        jScrollPane1.setViewportView(TableCon);

        LabelCon.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LabelCon.setText("Produto:");

        TextFieldCon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TextFieldConActionPerformed(evt);
            }
        });

        ButtonCon.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ButtonCon.setText("Consultar");
        ButtonCon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonConActionPerformed(evt);
            }
        });

        ButtonVoltar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ButtonVoltar.setText("Voltar");
        ButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonVoltarActionPerformed(evt);
            }
        });

        ButtonExcluir.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ButtonExcluir.setText("Excluir");
        ButtonExcluir.setMaximumSize(new java.awt.Dimension(91, 25));
        ButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonExcluirActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setText("Editar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(LabelCon, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(362, 362, 362))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(TextFieldCon, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(49, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(ButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(ButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(ButtonCon, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(labelHeader3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(64, 64, 64))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelHeader3, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelCon)
                    .addComponent(TextFieldCon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ButtonCon, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23))
        );

        pack();
    }// </editor-fold>                        

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        try {
            Class.forName("com.mysql.jdbc.Driver"); //busca uma classe pelo nome. Classe apontada � a de conex�o jdbc

            //cria��o de var�ave de conex�o mysql. Strings abaixo representam caminho de conex�o do mysql, user e senha
            Connection con;
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sti?useSSL=false", "root", "1234");

            //string de query para inser��o no banco de dados
            String query = "SELECT produtos (descricao, marca, quantidade) VALUES (?,?,?)";

            //recebe query de comando
            PreparedStatement ps = con.prepareStatement(query);

            //recebe as vari�veis dos TextField, faz update no banco e fecha conex�o
            ps.setString(1, TextFieldProduto.getText());
            ps.setString(2, TextFieldQtd.getText());
            ps.executeUpdate();
            ps.close();
            con.close();

        } catch (ClassNotFoundException ex) {
            System.out.println("Classe n�o encontrada");
            Logger.getLogger(CadView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException e){
            System.out.println(e);

        }

    }                                        

    private void TextFieldConActionPerformed(java.awt.event.ActionEvent evt) {                                             
        // TODO add your handling code here:
    }                                            

    private void ButtonConActionPerformed(java.awt.event.ActionEvent evt) {                                          
        try {
            //Class.forName("com.mysql.jdbc.Driver"); //busca uma classe pelo nome. Classe apontada � a de conex�o jdbc
            
            //cria��o de outra var�avel de conex�o mysql. Strings abaixo representam caminho de conex�o do mysql, user e senha
            Connection con = ConnectionFactory.getConnection();
            //con2 = DriverManager.getConnection("jdbc:mysql://localhost:3306/sti?useSSL=false", "root", "1234");
            
            String query;
            PreparedStatement ps2;
 
            if (TextFieldCon.getText().equals("tudo")){
                query = "SELECT * FROM produtos";
                ps2 = con.prepareStatement(query);
            }else{
                query =  "SELECT * FROM produtos WHERE descricao LIKE ?";
                ps2 = con.prepareStatement(query); //recebe o comando pela query
                ps2.setString(1,"%" + TextFieldCon.getText() + "%");
            }
            
            ResultSet rs;
            rs = ps2.executeQuery();
            
            //seta modelo default da tabela para zero linhas enquanto n�o tiver nada para mostrar
            DefaultTableModel modelo = (DefaultTableModel) TableCon.getModel();
            modelo.setNumRows(0);
            
            //novo objeto para popular a tabela com os dados pesquisados
            while (rs.next()){
                modelo.addRow(new Object[]{
                    rs.getString("id_Produtos"), rs.getString("descricao"), rs.getString("marca"),
                    rs.getString("quantidade")
                });
            }
        } catch (SQLException e2){
            System.out.println(e2);
        }
    }                                         

    private void ButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {                                              
        try {
            //Class.forName("com.mysql.jdbc.Driver"); //busca uma classe pelo nome. Classe apontada � a de conex�o jdbc
            
            //cria��o de outra var�avel de conex�o mysql. Strings abaixo representam caminho de conex�o do mysql, user e senha
            Connection con = ConnectionFactory.getConnection();
            //con3 = DriverManager.getConnection("jdbc:mysql://localhost:3306/sti?useSSL=false", "root", "1234");
            
            //query de comando sql
            String query = "DELETE FROM produtos WHERE id_Produtos = ?";
            
            PreparedStatement ps3 = con.prepareStatement(query); //recebe o comando pela query
            
            ps3.setString(1, TextFieldCon.getText()); //
            
            //vari�vel que pega a coluna selecionada para exclus�o
            String index;
            index = (String) TableCon.getModel().getValueAt(TableCon.getSelectedRow(), 0);
            
            ps3.setString(1, index);
            ps3.executeUpdate();
            
            ////////////////////////////////////////////////////
            
            query =  "SELECT * FROM produtos WHERE descricao LIKE ?";
                ps3 = con.prepareStatement(query); //recebe o comando pela query
                ps3.setString(1,"%" + TextFieldCon.getText() + "%");
            ResultSet rs;
            rs = ps3.executeQuery();
            
            //seta modelo default da tabela para zero linhas enquanto n�o tiver nada para mostrar
            DefaultTableModel modelo = (DefaultTableModel) TableCon.getModel();
            modelo.setNumRows(0);
            
            //novo objeto para popular a tabela com os dados pesquisados
            while (rs.next()){
                modelo.addRow(new Object[]{
                    rs.getString("id_Produtos"), rs.getString("descricao"), rs.getString("marca"),
                    rs.getString("quantidade")
                });
            }
        } catch (SQLException e3){
            System.out.println(e3);
        }
    }                                             

    private void ButtonVoltarActionPerformed(java.awt.event.ActionEvent evt) {                                             
        dispose();
        MenuView mv = new MenuView();
        mv.setVisible(true);
        mv.setLocationRelativeTo(null);
    }                                            

    // m�todo do bot�o de edi��o. Recebe cada linha da tabela numa string diferente
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        String produto = (String) TableCon.getModel().getValueAt(TableCon.getSelectedRow(), 1);
        String marca = (String) TableCon.getModel().getValueAt(TableCon.getSelectedRow(), 2);
        String quantidade = (String) TableCon.getModel().getValueAt(TableCon.getSelectedRow(), 3);
        
        dispose();
        CadView Cadv = new CadView(null, true, produto, marca, quantidade);
        Cadv.setLocationRelativeTo(null);
        Cadv.setVisible(true);
        
    }                                        

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultaView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ConsultaView dialog = new ConsultaView(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton ButtonCon;
    private javax.swing.JButton ButtonExcluir;
    private javax.swing.JButton ButtonVoltar;
    private javax.swing.JLabel LabelCon;
    private javax.swing.JLabel LabelProduto;
    private javax.swing.JLabel LabelQtd;
    private javax.swing.JTable TableCon;
    private javax.swing.JTextField TextFieldCon;
    private javax.swing.JTextField TextFieldProduto;
    private javax.swing.JTextField TextFieldQtd;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JScrollPane jScrollPane1;
    private java.awt.Label labelHeader;
    private java.awt.Label labelHeader1;
    private java.awt.Label labelHeader2;
    private java.awt.Label labelHeader3;
    // End of variables declaration                   
}
