package view;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Clock;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbc.ConnectionFactory;

/**
 * @author Justo
 */
public class CadView extends javax.swing.JDialog {

    /**
     * construtor form cadView - tela de cadastro
     */
    public CadView(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    
    //construtor da segunda chamada do formcadView para edi��o dos dados
    public CadView(java.awt.Frame parent, boolean modal, String Produto, String Marca, String Quantidade) {
        super(parent, modal);
        initComponents();
        
        this.TextFieldProduto.setText(Produto);
        this.TextFieldMarca.setText(Marca);
        this.TextFieldQtd.setText(Quantidade);
    }

    @SuppressWarnings("unchecked")
    //par�metros como fonte, tamanho e localiza��o dos bot�es e labels na tela
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        labelHeader = new java.awt.Label();
        ButtonVoltar = new javax.swing.JButton();
        ButtonSalvar = new javax.swing.JButton();
        LabelProduto = new javax.swing.JLabel();
        LabelQtd = new javax.swing.JLabel();
        TextFieldProduto = new javax.swing.JTextField();
        TextFieldQtd = new javax.swing.JTextField();
        LabelMarca = new javax.swing.JLabel();
        TextFieldMarca = new javax.swing.JTextField();
        LabelCod = new javax.swing.JLabel();
        TextFieldCod = new javax.swing.JTextField();

        jTextField1.setText("jTextField1");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jLabel1.setText("jLabel1");

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setForeground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(580, 300));

        labelHeader.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        labelHeader.setName("header"); // NOI18N
        labelHeader.setText("Sistema Gerenciador de Estoque");

        ButtonVoltar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ButtonVoltar.setText("Voltar");
        ButtonVoltar.setActionCommand("jButton3");
        ButtonVoltar.setPreferredSize(new java.awt.Dimension(42, 120));
        ButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonVoltarActionPerformed(evt);
            }
        });

        ButtonSalvar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ButtonSalvar.setText("Salvar");
        ButtonSalvar.setActionCommand("jButton5");
        ButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonSalvarActionPerformed(evt);
            }
        });

        LabelProduto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LabelProduto.setText("Produto:");

        LabelQtd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LabelQtd.setText("Quantidade:");

        TextFieldProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TextFieldProdutoActionPerformed(evt);
            }
        });

        LabelMarca.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LabelMarca.setText("Marca:");

        TextFieldMarca.setToolTipText("");

        LabelCod.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LabelCod.setText("C�digo:");

        TextFieldCod.setEnabled(false);
        TextFieldCod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TextFieldCodActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(LabelMarca)
                    .addComponent(LabelCod))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(TextFieldCod, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(LabelProduto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TextFieldProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(TextFieldMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(LabelQtd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(TextFieldQtd, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(ButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(93, 93, 93)
                .addComponent(labelHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(97, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelCod)
                    .addComponent(TextFieldCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LabelProduto)
                    .addComponent(TextFieldProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelMarca)
                    .addComponent(TextFieldMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LabelQtd)
                    .addComponent(TextFieldQtd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(49, 49, 49)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>                        

    private void ButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {                                             
        try {
            //cria��o de conex�o jdbc
            Connection con = ConnectionFactory.getConnection();
            
            //string de query para inser��o ou update no banco de dados
            String query;
            PreparedStatement ps;
            if (TextFieldCod.getText().equals("")){
                query = "INSERT INTO produtos (descricao, marca, quantidade) VALUES (?,?,?)";
                ps = con.prepareStatement(query);//recebe query de comando
                
                //recebe as vari�veis dos TextField
                ps.setString(1, TextFieldProduto.getText());
                ps.setString(2, TextFieldMarca.getText());
                ps.setString(3, TextFieldQtd.getText());
                
            } else {
                query = "UPDATE produtos SET descricao = ?, marca = ?, quantidade = ? WHERE id_Produtos = ?";
                ps = con.prepareStatement(query);//recebe query de comando
                
                //recebe as vari�veis dos TextField
                ps.setString(1, TextFieldProduto.getText());
                ps.setString(2, TextFieldMarca.getText());
                ps.setString(3, TextFieldQtd.getText());
            }

            //faz update no banco e fecha conex�o
            ps.executeUpdate();
            ps.close();
            con.close();
            
            //gambiarra para dar "clear" na tela - (descobrir jeito mais bonito depois)
            dispose();
            CadView cadv = new CadView(null, true);
            cadv.setLocationRelativeTo(null);
            cadv.setVisible(true);
            
        } catch (SQLException e){
            System.out.println(e);            
            
        }
        
    }                                            

    private void ButtonVoltarActionPerformed(java.awt.event.ActionEvent evt) {                                             
        dispose();
        MenuView mv = new MenuView();
        mv.setVisible(true);
        mv.setLocationRelativeTo(null);
    }                                            

    private void TextFieldCodActionPerformed(java.awt.event.ActionEvent evt) {                                             
        // TODO add your handling code here:
    }                                            

    private void TextFieldProdutoActionPerformed(java.awt.event.ActionEvent evt) {                                                 
        // TODO add your handling code here:
    }                                                

    /**
     * @param args the command line arguments
     * main para executar fun��es da tela com bot�es
     */
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* cria e mostra o dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                CadView dialog = new CadView(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Declara��o de vari�veis de classe J (Jbutton, Frame, panel, etc)
    // Variables declaration - do not modify                     
    private javax.swing.JButton ButtonSalvar;
    private javax.swing.JButton ButtonVoltar;
    private javax.swing.JLabel LabelCod;
    private javax.swing.JLabel LabelMarca;
    private javax.swing.JLabel LabelProduto;
    private javax.swing.JLabel LabelQtd;
    private javax.swing.JTextField TextFieldCod;
    private javax.swing.JTextField TextFieldMarca;
    private javax.swing.JTextField TextFieldProduto;
    private javax.swing.JTextField TextFieldQtd;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField jTextField1;
    private java.awt.Label labelHeader;
    // End of variables declaration                   
}
