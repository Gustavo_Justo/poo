package view;

/**
 * @author Justo
 */
public class Principal {
    
    public static void main(String[] args) {
        
        //instancia a classe de menu (form pai)
       MenuView mv = new MenuView();
       mv.setVisible(true);
       mv.setLocationRelativeTo(null);

    }    
}
